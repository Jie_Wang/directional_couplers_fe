#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#
# SPDX-License-Identifier: GPL-3.0
#
# GNU Radio Python Flow Graph
# Title: Interference_signal
# Author: Jie_Wang
# GNU Radio version: 3.8.2.0

from gnuradio import analog
from gnuradio import blocks
from gnuradio import gr
from gnuradio.filter import firdes
import sys
import signal
from argparse import ArgumentParser
from gnuradio.eng_arg import eng_float, intx
from gnuradio import eng_notation
from gnuradio import uhd
import time


class Interference_signal(gr.top_block):

    def __init__(self, const=0.05, gain=70, samp_rate=2e6, tx_freq=2.4e9):
        gr.top_block.__init__(self, "Interference_signal")

        ##################################################
        # Parameters
        ##################################################
        self.const = const
        self.gain = gain
        self.samp_rate = samp_rate
        self.tx_freq = tx_freq

        ##################################################
        # Blocks
        ##################################################
        self.uhd_usrp_sink_1 = uhd.usrp_sink(
            ",".join(("", "")),
            uhd.stream_args(
                cpu_format="fc32",
                args='',
                channels=list(range(0,1)),
            ),
            '',
        )
        self.uhd_usrp_sink_1.set_center_freq(tx_freq, 0)
        self.uhd_usrp_sink_1.set_gain(gain, 0)
        self.uhd_usrp_sink_1.set_antenna('TX/RX', 0)
        self.uhd_usrp_sink_1.set_bandwidth(0.5*samp_rate, 0)
        self.uhd_usrp_sink_1.set_samp_rate(samp_rate)
        self.uhd_usrp_sink_1.set_time_unknown_pps(uhd.time_spec())
        self.blocks_multiply_const_vxx_0 = blocks.multiply_const_cc(const)
        self.blocks_file_sink_0 = blocks.file_sink(gr.sizeof_gr_complex*1, 'tx1.txt', False)
        self.blocks_file_sink_0.set_unbuffered(True)
        self.analog_sig_source_x_0 = analog.sig_source_c(samp_rate, analog.GR_CONST_WAVE, tx_freq, 1, 0, 0)



        ##################################################
        # Connections
        ##################################################
        self.connect((self.analog_sig_source_x_0, 0), (self.blocks_multiply_const_vxx_0, 0))
        self.connect((self.blocks_multiply_const_vxx_0, 0), (self.blocks_file_sink_0, 0))
        self.connect((self.blocks_multiply_const_vxx_0, 0), (self.uhd_usrp_sink_1, 0))


    def get_const(self):
        return self.const

    def set_const(self, const):
        self.const = const
        self.blocks_multiply_const_vxx_0.set_k(self.const)

    def get_gain(self):
        return self.gain

    def set_gain(self, gain):
        self.gain = gain
        self.uhd_usrp_sink_1.set_gain(self.gain, 0)

    def get_samp_rate(self):
        return self.samp_rate

    def set_samp_rate(self, samp_rate):
        self.samp_rate = samp_rate
        self.analog_sig_source_x_0.set_sampling_freq(self.samp_rate)
        self.uhd_usrp_sink_1.set_samp_rate(self.samp_rate)
        self.uhd_usrp_sink_1.set_bandwidth(0.5*self.samp_rate, 0)

    def get_tx_freq(self):
        return self.tx_freq

    def set_tx_freq(self, tx_freq):
        self.tx_freq = tx_freq
        self.analog_sig_source_x_0.set_frequency(self.tx_freq)
        self.uhd_usrp_sink_1.set_center_freq(self.tx_freq, 0)




def argument_parser():
    parser = ArgumentParser()
    parser.add_argument(
        "-c", "--const", dest="const", type=eng_float, default="50.0m",
        help="Set const [default=%(default)r]")
    parser.add_argument(
        "-g", "--gain", dest="gain", type=eng_float, default="70.0",
        help="Set gain [default=%(default)r]")
    parser.add_argument(
        "-s", "--samp-rate", dest="samp_rate", type=eng_float, default="2.0M",
        help="Set samp_rate [default=%(default)r]")
    parser.add_argument(
        "-f", "--tx-freq", dest="tx_freq", type=eng_float, default="2.4G",
        help="Set tx_freq [default=%(default)r]")
    return parser


def main(top_block_cls=Interference_signal, options=None):
    if options is None:
        options = argument_parser().parse_args()
    tb = top_block_cls(const=options.const, gain=options.gain, samp_rate=options.samp_rate, tx_freq=options.tx_freq)

    def sig_handler(sig=None, frame=None):
        tb.stop()
        tb.wait()

        sys.exit(0)

    signal.signal(signal.SIGINT, sig_handler)
    signal.signal(signal.SIGTERM, sig_handler)

    tb.start()

    try:
        while True:
            if time.time() - currentTime >= numdelta:
                tb.stop()
                tb.wait()
                print('[Incident Signal] End Transmission')
                return
    except EOFError:
        pass


if __name__ == '__main__':
    main()
