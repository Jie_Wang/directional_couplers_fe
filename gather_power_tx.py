#!/bin/bash
import os
import sys
import time
import subprocess
from multiprocessing import Process

'''
Currently this tutorial is set up to gather the power of *1*
reception radios and *1* transmision radio. This is what I am currently
using for my research, but it is very simple to add or subtract reception
radios as you please. This tutorial works best with a single tranmission radio

Gather_power.py requires you to either copy or delete connections based
on how many reception radios you want.
  In the receive() method:
      Copy or delete the code block for each connection you want
          "connection1.run('uhd_rx_cfile -f 3555e6 --lo-offset=1.2M -N 10000 test_bin_power#')
          connection1.run('iq_to_power.py -p ~/test_bin_power# -w 5000 -n bin_db_power#')
          subprocess.call('pscp -pw *SSH PASSWORD* -scp *USERNAME*@pc*PC NUMBER*.emulab.net:~/bin_db_power#.csv gather_power#.csv')"
      Make sure to change all the appropriate numbers: connection#, test_bin_power#,
      bin_db_power#, and gather_power#.csv
  Setting up the connection:
      Copy or delete the code block for each connection you want
          "receive_ssh_# = Connection(host='pc*PC NUMBER*.emulab.net', user='*USERNAME*',
           connect_kwargs={'key_filename': '*ADD KEY LOCATION*'})"
      Make sure to change all the appropriate numbers: receive_ssh_#,
  Calling the receive() method:
      Edit the parameter of the receive() function to match the connections
      just made, appropriately add, deleting, and changing numbers.
'''

def transmit(freq_tx):
    print('start transmit-tx: \n')
    currentTime = time.time()
    # runs uhd_siggen_20sec, transmits specified signal for 20 seconds
    # subprocess.run('uhd_siggen_20sec --const --gain 60 --freq {} --amplitude 1 -v -s 2e6'.format(freq_tx))
    # OFDM signal Popen
    subprocess.run(['OFDM_TX.py', '-g 60', '-s 2e6', '-t 18000', '-f {}'.format(freq_tx)])
    
    with open('center_freq_tx.txt','a+') as f:
        f.write(str(freq_tx)+' ')
    f.close()
    
    print('end transmit-tx!') #--gain 76
    print(time.time()-currentTime)

def main():
    # initialization
    Freq_start = 3550e6 # start of the CBRS band
    Freq_end = 3700e6 # end of the CBRS band
    Interval = 30e6 # bandwidth


    for m in range(int((Freq_end-Freq_start)/Interval)):
        # we'd like to see how the separation setults will change as center_freq changes.
        Center_freq = Freq_start + m*Interval + Interval/2
        Freq_tx = Freq_start + m*Interval + Interval/2 - 8e6
        print('Freq_tx',Freq_tx)
        transmit(Freq_tx)
        # sleeping for 15 seconds allows the tranmission and reception
        time.sleep(30)

    print('TX done')
# In order to transmit and receive simultaneously, multiprocessing must be used
if __name__ == '__main__':
    main()
# subprocess.run: execute a command and wait for it to finish
# subprocess.popen: continue doing stuff while the process finishes