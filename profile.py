#!/usr/bin/env python

"""
Allocate one or more B210 radios from a single fixed endpoint site (freq limitation optional)
"""

#
import geni.portal as portal
import geni.rspec.pg as rspec
import geni.rspec.emulab as elab
import geni.rspec.igext as IG
import geni.urn as URN


tourDescription = """
**Allocate one or more B210 radios from a single fixed endpoint site (freq limitation optional)**

""";

tourInstructions = """

After instantiation log into the node and run '/opt/rfnoc/setupenv.sh' to pull in the GNU Radio environment.  Be sure you are doing this from within a 'bash' shell.

""";

#
# Globals
#
class GLOBALS(object):
    GR_IMAGE = "urn:publicid:IDN+emulab.net+image+emulab-ops:UBUNTU18-64-GR38-PACK"


setup_command = "/local/repository/startup.sh"
installs = ["gnuradio"]

def b210_nuc_pair(idx,aggregate, b210_node):
    # Node n1
    n1 = request.RawPC("b210-%s-%s"%(aggregate, b210_node.NodeID))
    agg_full_name = "urn:publicid:IDN+%s.powderwireless.net+authority+cm"\
                    %(aggregate)
    n1.component_manager_id = agg_full_name
    n1.component_id = b210_node.NodeID  # Hardwire as this is what is connected to antenna.
    n1.disk_image = GLOBALS.GR_IMAGE

    service_command = " ".join([setup_command] + installs)
    n1.addService(rspec.Execute(shell="bash", command=service_command))

# Create a portal context.
pc = portal.Context()

# Create a Request object to start building the RSpec.
request = pc.makeRequestRSpec()


# Profile parameters.
agglist = [
    ("test1","TEST 1"),
    ("bus-test2","BUS-TEST2"),
    ("web", "WEB"),
    ("ebc", "EBC"),
    ("bookstore", "Bookstore"),
    ("humanities", "Humanities"),
    ("law73", "Law 73"),
    ("madsen", "Madsen"),
    ("sagepoint", "Sage Point"),
    ("moran", "Moran"),
    ("cpg", "Garage"),
    ("guesthouse", "Guesthouse")
]

pc.defineParameter("aggregate","Fixed Endpoint Site",portal.ParameterType.STRING,
                   agglist[0],agglist)

# Node type parameter for PCs to be paired with B210 radios.
# Restricted to those that are known to work well with them.
Compute_node = [("nuc1", "nuc1"),
                ("nuc2", "nuc2"),
                ("ed1", "ed1"),
                ("ed2", "ed2")

]

pc.defineStructParameter("b210_nodes", "B210 Radios", [],
                                     multiValue=True,
                                     min=0, max=None,
                                     members=[
                                         portal.Parameter(
                                             "NodeID",
                                             "Fixed Endpoint B210",
                                             portal.ParameterType.STRING,
                                             Compute_node[0],
                                             Compute_node)
                                     ],
                                    )

# Frequency/spectrum parameters
pc.defineStructParameter(
    "freq_ranges", "Range", [],
    multiValue=True,
    min=0,
    multiValueTitle="Frequency ranges for over-the-air operation.",
    members=[
        portal.Parameter(
            "freq_min",
            "Frequency Min",
            portal.ParameterType.BANDWIDTH,
            2400,
            longDescription="Values are rounded to the nearest kilohertz."
        ),
        portal.Parameter(
            "freq_max",
            "Frequency Max",
            portal.ParameterType.BANDWIDTH,
            6000.0,
            longDescription="Values are rounded to the nearest kilohertz."
        ),
    ])

params = pc.bindParameters()

for i, frange in enumerate(params.freq_ranges):
    if frange.freq_min < 2400 or frange.freq_min > 6000 \
       or frange.freq_max < 2400 or frange.freq_max > 6000:
        perr = portal.ParameterError("Frequencies must be between 3400 and 3800 MHz", ["freq_ranges[%d].freq_min" % i, "freq_ranges[%d].freq_max" % i])
        portal.context.reportError(perr)
    if frange.freq_max - frange.freq_min < 1:
        perr = portal.ParameterError("Minimum and maximum frequencies must be separated by at least 1 MHz", ["freq_ranges[%d].freq_min" % i, "freq_ranges[%d].freq_max" % i])
        portal.context.reportError(perr)


pc.verifyParameters()

# Request frequency range(s)
for frange in params.freq_ranges:
    request.requestSpectrum(frange.freq_min, frange.freq_max, 100)

for i, b210_node in enumerate(params.b210_nodes):
    b210_nuc_pair(i, params.aggregate, b210_node)


# Final rspec.
pc.printRequestRSpec(request)